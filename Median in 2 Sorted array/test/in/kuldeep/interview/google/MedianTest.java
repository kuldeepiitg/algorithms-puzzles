package in.kuldeep.interview.google;

import org.junit.Test;

public class MedianTest {

	@Test
	public void testFindMedian() {
		
		int[] firstSortedArray = new int[]{7,8,9,10,11,12};
		int[] secondSortedArray = new int[]{1,2,3,4,5,6};
		assert 6.5D == Median.findMedian(firstSortedArray, secondSortedArray);
		assert 7.0D == Median.findMedian(new int[]{1,11}, new int[]{2,3, 11,12});
		assert 31.5D == Median.findMedian(new int[]{0,10,20,30,40,50}, new int[]{1,2,33, 35,40,50});
		assert 6.0D == Median.findMedian(new int[]{0,1,2,3,4,5}, new int[]{7,8,9,10,11,12});
		assert 33.0D == Median.findMedian(new int[]{0,10,20,30,40,50}, new int[]{1,2,33,33,35,40,50,51});
		assert 0.0D == Median.findMedian(new int[]{0,0}, new int[]{0,0});
		assert 2.5D == Median.findMedian(new int[]{1,2}, new int[]{3,4});
		assert 2.0D == Median.findMedian(new int[]{1}, new int[]{3});
		assert 0.0D == Median.findMedian(new int[]{0}, new int[]{0});
		assert 4.0D == Median.findMedian(new int[]{1,2,3}, new int[]{5,6,7});
		assert 5.5D == Median.findMedian(new int[]{1,2,8}, new int[]{5,6,7});
		assert 6.5D == Median.findMedian(new int[]{1,2,8,10,23}, new int[]{5,6,7});

		assert 5.0D == Median.findMedian(new int[]{1,3,5,7,9}, new int[]{2,4,6,8});
		assert 5.0D == Median.findMedian(new int[]{1, 2, 3, 4, 5}, new int[]{6, 7, 8, 9});
		assert 5.0D == Median.findMedian(new int[]{1, 2, 3, 4}, new int[]{5, 6, 7, 8, 9});
		assert 2.0D == Median.findMedian(new int[]{1,2}, new int[]{3});
		assert 2.0D == Median.findMedian(new int[]{1}, new int[]{2,3});
		assert 3.0D == Median.findMedian(new int[]{1,5,6}, new int[]{2,3});
		assert 4.0D == Median.findMedian(new int[]{1,2,6}, new int[]{3,4,7,9});
	}

}
