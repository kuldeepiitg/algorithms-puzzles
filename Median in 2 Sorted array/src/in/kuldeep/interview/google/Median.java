package in.kuldeep.interview.google;

/**
 * Median of two sorted arrays.
 * 
 * @author kuldeep
 */
public class Median {

	// FIXME: 11/05/20 Incorrect algorithm, need to rethink
	public static double findMedian(int[] firstSortedArray, int[] secondSortedArray) {

		if ((firstSortedArray.length + secondSortedArray.length)%2 == 0) {
			return findMedianForEvenCount(firstSortedArray, secondSortedArray);
		} else {
			return findMedianForOddCount(firstSortedArray, secondSortedArray);
		}
	}

	private static double findMedianForEvenCount(int[] first, int[] second) {

		// Keep first array always shorter
		int[] temp;
		if (second.length < first.length) {
			temp = second;
			second = first;
			first = temp;
		}

		int leftEdge = 0;
		int rightEdge = first.length - 1;
		int medianOffset = (first.length + second.length - 1)/2;

		int firstMarker = (first.length - 1)/2;
		int secondMarker = (medianOffset - (firstMarker + 1));

		while (areArraysNotPartitionedWell(first, second, firstMarker, secondMarker)) {

			if (first[firstMarker] <= second[secondMarker]) {
				// move first marker right, increase
				leftEdge = firstMarker + 1;
			} else {
				// move first marker left, decrease
				rightEdge = firstMarker - 1;
			}

			// hack: check if need to handle leftEdge == first.length - 1
			if (rightEdge == -1) {
				firstMarker = -1;
			} else {
				firstMarker = (leftEdge + rightEdge)/2;
			}

			secondMarker = (medianOffset - (firstMarker + 1));
			if (firstMarker == first.length - 1 || firstMarker == -1) {
				break;
			}
		}

		if (firstMarker == -1) {
			// if both arrays are equal in length
			if (secondMarker == second.length - 1) {
				return ((double) second[secondMarker] + (double) first[0])/2;
			}

			return ((double) second[secondMarker] + (double) Math.min(first[0],second[secondMarker + 1]))/2;
		} else if (firstMarker == first.length - 1) {
			// if both arrays are equal in length
			if (secondMarker == -1) {
				return ((double) first[firstMarker] + (double) second[secondMarker + 1])/2;
			}

			return ((double) Math.max(first[firstMarker], second[secondMarker]) + (double) second[secondMarker + 1])/2;
		}

		int maxOfLefts = Math.max(first[firstMarker], second[secondMarker]);
		int minOfRights = Math.min(first[firstMarker + 1], second[secondMarker + 1]);
		return ((double)maxOfLefts + (double) minOfRights) / 2;
	}

	/**
	 * Find median when total count of elements is odd
	 * @param first first array
	 * @param second second array
	 * @return median of single merged array
	 */
	private static double findMedianForOddCount(int[] first, int[] second) {

		// always take smaller array as first, we will perform binary search in first
		// and place marker in second such that element count of left sub-arrays is equal to right
		if (first.length > second.length) {
			int[] temp = first;
			first = second;
			second = temp;
		}
		// index of median element in single merged arrays
		int medianIndex = (first.length + second.length)/2;

		// start binary search from rightmost end
		int firstMarker = first.length - 1;
		int secondMarker = medianIndex - (firstMarker + 1);
		int leftEdge = -1;
		int rightEdge = firstMarker - 1;

		while (areArraysNotPartitionedWell(first, second, firstMarker, secondMarker)) {

			// if arrays are not partitioned well, then it must be because max of lefts is more than min of rights
			if (first[firstMarker] > second[secondMarker]) {
				// If max belong to first left, then first left is needed to be decreased in size
				// move left
				if (firstMarker >= 0) {
					rightEdge = firstMarker - 1;
				} else {
					rightEdge = -1;
				}
			} else {
				// If max belong to second left, then first left is needed to be increased in size so that
				// second left can get reduced accordingly
				// move right
				if (firstMarker < first.length - 1) {
					leftEdge = firstMarker + 1;
				} else {
					leftEdge = first.length - 1;
				}
			}

			// Caution check
			if (leftEdge > rightEdge) {
				throw new RuntimeException(String.format("Left edge should not be more than right, left:%s, right:%s", leftEdge, rightEdge));
			}

			// If right edge have reached behind 0, this mean all elements of second array are smaller than first
			if (rightEdge == -1) {
				firstMarker = -1;
				secondMarker = medianIndex;
				break;
			}

			firstMarker = (leftEdge + rightEdge)/2;
			secondMarker = medianIndex - (firstMarker + 1);
		}

		if (firstMarker == -1) {
			// all elements of first are larger, then median lie in second
			return second[secondMarker];
		} else if (firstMarker == first.length - 1 && secondMarker == -1) {
			// all elements of first are smaller and sizes of arrays are equal, then median lie in first.
			// But it is never possible as odd and even numbers can't be equal
			return first[firstMarker];
		} else {
			if (first[firstMarker] < second[secondMarker]) {
				return second[secondMarker];
			} else {
				return first[firstMarker];
			}
		}
	}

	/**
	 * Check if maximum of rightmost elements of both left segments is less than minimum of leftmost elements of both
	 * right segments. Both the markers together divide elements into two sets. Here first set means all the elements
	 * left to firstMarker and secondMarker in first and second array respectively. Similarly, second set is collection
	 * of right sub-arrays.
	 *  e.g first: [2,3,4,5]
	 *  	second: [1,2,3,4,5,6,7,8,9]
	 * 		firstMarker: 2
	 * 		secondMarker: 3
	 * 		Left set: {1,2,2,3,3,4,4}
	 * 		Right set: {5,5,6,7,8,9}
	 *
 	 * @param first first sorted array
	 * @param second second sorted array
	 * @param firstMarker marker in first array to partition into two
	 * @param secondMarker marker in second array to partition into two
	 * @return true if max of left set is smaller than min of right set
	 */
	private static boolean areArraysNotPartitionedWell(int[] first, int[] second, int firstMarker, int secondMarker) {

		// max element of left sub-array of first
		int firstLeft = Integer.MIN_VALUE;
		if (firstMarker >= 0 && firstMarker < first.length) {
			firstLeft = first[firstMarker];
		}

		// max element of left sub-array of second
		int secondLeft = Integer.MIN_VALUE;
		if (secondMarker >= 0 && secondMarker < second.length) {
			secondLeft = second[secondMarker];
		}

		// max element of both the left sub-arrays merged together
		int maxOfLefts = Math.max(firstLeft, secondLeft);

		// min element of right sub-array of first
		int firstRight = Integer.MAX_VALUE;
		if (firstMarker >= 0 && firstMarker < first.length - 1) {
			firstRight = first[firstMarker + 1];
		}

		// min element of right sub-array of second
		int secondRight = Integer.MAX_VALUE;
		if (secondMarker >= 0 && secondMarker < second.length - 1) {
			secondRight = second[secondMarker + 1];
		}

		// min element of both the right sub-arrays merged together
		int minOfRights = Math.min(firstRight, secondRight);

		// check if each element in left partition are smaller than each element in right partition
		return maxOfLefts > minOfRights;
	}

	public static float findMedianBruteForce(int[] firstSortedArray, int[] secondSortedArray) {

		int[] mergedArray = new int[firstSortedArray.length+secondSortedArray.length];

		int firstIndex = 0;
		int secondIndex = 0;
		int index = 0;
		while (firstIndex < firstSortedArray.length && secondIndex < secondSortedArray.length) {
			if (firstSortedArray[firstIndex] <= secondSortedArray[secondIndex]) {
				mergedArray[index] = firstSortedArray[firstIndex];
				firstIndex++;
			} else {
				mergedArray[index] = secondSortedArray[secondIndex];
				secondIndex++;
			}
			index++;
		}
		while (firstIndex < firstSortedArray.length) {
			mergedArray[index] = firstSortedArray[firstIndex];
			firstIndex++;
			index++;
		}
		while (secondIndex < secondSortedArray.length) {
			mergedArray[index] = secondSortedArray[secondIndex];
			secondIndex++;
			index++;
		}

		if (mergedArray.length%2 == 1) {
			return mergedArray[(mergedArray.length - 1)/2];
		} else {
			return ((float)mergedArray[mergedArray.length/2] + (float)mergedArray[mergedArray.length/2 + 1])/2;
		}
	}
}
