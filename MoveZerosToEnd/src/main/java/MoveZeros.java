public class MoveZeros {

    public static int[] move(int[] array) {

        int i = 0;
        while (i<array.length && array[i] != 0) {
            i++;
        }

        int j = i+1;
        while (j<array.length) {
            if (array[j] != 0) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
            }
            j++;
        }

        return array;
    }
}
