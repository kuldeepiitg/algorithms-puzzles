package com.tarkshala.regex;

public class Solution {

    public static void main(String[] args) {
        RegexAutomata regexAutomata = new RegexAutomata();
        regexAutomata.populateRegexIntoAutomata("ab*a*c*a");
        if (regexAutomata.match("aaba", 0)) {
            System.out.println("Matched");
        } else {
            System.out.println("No match");

        }
    }
}
