package com.tarkshala.regex;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;

public class RegexAutomata {

    private State[] states;

    public void populateRegexIntoAutomata(String regex) {

        int stateCount = 1;
        for (char symbol: regex.toCharArray()) {
            if (symbol == '*') {
                stateCount--;
            } else {
                stateCount++;
            }
        }

        states = new State[stateCount];
        for (int i = 0; i < states.length; i++) {
            states[i] = new State(i);
        }


        int currentState = 0;
        char[] symbols = regex.toCharArray();
        for (int i = 0; i < symbols.length; i++) {
            if (i+1 < symbols.length && symbols[i+1] == '*') {
                states[currentState].addRule(new Rule(symbols[i], currentState));
                i++;
            } else {
                states[currentState].addRule(new Rule(symbols[i], currentState + 1));
                currentState++;
            }
        }
    }

    @Getter
    @Builder
    @AllArgsConstructor
    public static class State {
        private int label;
        private ArrayList<Rule> rules;

        public State(int label) {
            this.label = label;
            this.rules = new ArrayList<>();
        }

        public void addRule(Rule rule) {
            rules.add(rule);
        }
    }

    @Getter
    @Builder
    @AllArgsConstructor
    public static class Rule {
        private char symbol;
        private int destinationState;
    }

    public boolean match(String word, int currentState) {

        if (word.length() == 0 && states.length - 1 == currentState) {
            return true;
        } else if (word.length() == 0 && states.length - 1 != currentState) {
            return false;
        } else if (word.length() != 0 && states.length - 1 == currentState) {
            return false;
        }

        ArrayList<Rule> rules = states[currentState].getRules();
        for (int i = rules.size() - 1; i >= 0; i--) {
            if (word.charAt(0) == rules.get(i).symbol || rules.get(i).symbol == '.') {
                if (match(word.substring(1), rules.get(i).destinationState)) {
                    return true;
                }
            }
        }
        return false;
    }
}
