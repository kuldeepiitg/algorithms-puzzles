package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        int count = Solution.subarraysWithKDistinct(new int[]{1, 2, 1, 2, 3}, 2);
        System.out.println(count);
    }
}
