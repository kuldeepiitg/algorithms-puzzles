package com.tarkshala;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static int subarraysWithKDistinct(int[] nums, int k) {

        Map<Integer, Integer> frequency = new HashMap();

        int i = 0;
        int j = 0;

        int count = 0;

        while (j < nums.length) {

            while (frequency.size() < k) {
                if (j == nums.length) {
                    break;
                }
                if (!frequency.containsKey(nums[j])) {
                    if (frequency.size() == k-1) {
                        frequency.put(nums[j], 1);
                        break;
                    } else if (frequency.size() < k-1) {
                        frequency.put(nums[j], 1);
                    } else {
                        throw new RuntimeException("map size should not > k");
                    }
                } else {
                    frequency.put(nums[j], frequency.get(nums[j]) + 1);
                }
                j++;
            }

            while (frequency.size() == k) {
                count++;
                j++;
                if (j == nums.length) {
                    break;
                }
                if (frequency.containsKey(nums[j])) {
                    frequency.put(nums[j], frequency.get(nums[j]) + 1);
                } else {
                    break;
                }
            }

            while (frequency.size() == k) {
                if (i == j) {
                    break;
                }
                if (frequency.get(nums[i]) == 1) {
                    frequency.remove(nums[i]);
                    i++;
                    break;
                } else {
                    frequency.put(nums[i], frequency.get(nums[i]) - 1);
                }
                i++;
                count++;
            }
        }

        return count;
    }
}