Delete Edge to minimize subtree sum difference
========
Given an undirected tree whose each node is associated with a weight. We need to delete an edge in such a way that difference between sum of weight in one subtree to sum of weight in other subtree is minimized.

https://www.geeksforgeeks.org/delete-edge-minimize-subtree-sum-difference/


