package com.tarkshala.alogorithm;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

public class Graph {

    private int[] nodes;
    private ArrayList<Integer>[] edges;
    private int totalWeight = 0;

    private int[] leastDifferenceEdge;
    private int leastDifference;
    private boolean[] alreadyVisited;

    public Graph(int[] nodes) {
        this.nodes = nodes;
        this.edges = new ArrayList[nodes.length];
        for (int weight: nodes) {
            this.totalWeight += weight;
        }
        this.alreadyVisited = new boolean[nodes.length];
        this.leastDifference = Integer.MAX_VALUE;
        this.leastDifferenceEdge = new int[2];
    }

    private void addEdge(int from, int to) {
        if (this.edges[from] == null) {
            this.edges[from] = new ArrayList<>();
        }
        if (this.edges[to] == null) {
            this.edges[to] = new ArrayList<>();
        }
        this.edges[from].add(to);
        this.edges[to].add(from);
    }

    public void addEdges(int from, int[] to) {
        for (int terminalNode: to) {
            addEdge(from, terminalNode);
        }
    }

    private int visit(int from, int index) {
        this.alreadyVisited[index] = true;
        int subTreeWeight = nodes[index];
        for (int neighbor: edges[index]) {
            if (alreadyVisited[neighbor]) continue;
            subTreeWeight += visit(index, neighbor);
        }
        if (from != -1 && Math.abs(totalWeight - 2 * subTreeWeight) < leastDifference) {
            leastDifference = totalWeight - 2 * subTreeWeight;
            leastDifferenceEdge[0] = from;
            leastDifferenceEdge[1] = index;
        }
        return subTreeWeight;
    }

    public int[] findMinDifference() {
        int startIndex = 0;
        while (edges[startIndex].size() != 1) {
            startIndex++;
        }
        visit(-1, startIndex);
        return leastDifferenceEdge;
    }
}
