package com.tarkshala.alogorithm;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // construct graph
        Graph graph = new Graph(new int[]{4,2,1,6,3,5,2});
        graph.addEdges(0, new int[]{1,2,3});
        graph.addEdges(2, new int[]{4,5});
        graph.addEdges(3, new int[]{6});

        int[] leastDifferenceEdge = graph.findMinDifference();
        System.out.println(String.format("The least difference cut should be done on the edge: [%s, %s]", leastDifferenceEdge[0], leastDifferenceEdge[1]));
    }
}
