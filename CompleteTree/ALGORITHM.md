INSERTION IN COMPLETE BINARY TREE
====

Statement:
----------
A <b>Complete Binary Tree</b> is a binary tree that can be represented by an array having relation in elements as
"element at index 2*n and 2*n+1 are left and right child of element at n, respectively." And array is contiguously 
filled without any hole/empty. In other words, you can say that tree is uniformly filled at every level except leaf 
level. Whereas, at leaf level it can be completely filled or partially. If filled partially, then all the nodes must be
in left at leaf level.

Insert a node in the tree, such that it holds the completeness property after insertion too.


Approach 1 (Brute Force):
-----------
Do a depth first traversal or BFT.

Time: O(log(n)) + O(log(n)) + O(n) => O(n)
Space: O(log(n)) [for DFT] and O(n/2) [For BFT]


Approach 2 (Optimized):
-----------
Pseudo code:

1. Keep iterating on the left edge until we hit leaf, to find left depth.
2. Keep iterating on the right edge until we hit leaf, to find right depth.
3. If both are equal, insert the node as left leaf of leftmost child.
4. If left depth is one more than the  right depth
   1. Find the parent to insert the child at

To find correct parent:
    Move to left by one step and keep iterating to right of the subtree. If depth is equal to left depth.
It means this subtree is complete and right subtree of root have unoccupied place. 
Keep repeating process recursively to find correct place.

Time: O(log(n)^2)
SPace: O(1)
