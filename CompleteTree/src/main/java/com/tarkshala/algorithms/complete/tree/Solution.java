package com.tarkshala.algorithms.complete.tree;

public class Solution {

    public static void main(String[] args) {
        Node root = new Node(1);

        for (int i = 2; i <= 19; i++) {
            insert(root, i);
        }
        insert(root, 20);
        insert(root, 21);

        System.out.printf("Insertion done");
    }

    public static void insert(Node root, int toInsert) {

        int depth = getLeftDepth(root);
        int rightDepth = getRightDepth(root);

        if (depth == rightDepth) {
            insertBelowLeftMostLeaf(root, toInsert);
            return;
        } else if (depth == rightDepth + 1) {
            insert(root, toInsert, depth);
            return;
        } else {
            throw new RuntimeException("Right depth should not be more than left depth");
        }

    }

    private static void insert(Node root, int toInsert, int depth) {

        if (depth == 1) {
            throw new RuntimeException("Should not have reached here, as insertion should have been handled at depth = 2, node: " + root.value);
        } else if (depth == 2) {
            if (root.left == null) {
                root.left = new Node(toInsert);
            } else if (root.right == null) {
                root.right = new Node(toInsert);
            } else {
                throw new RuntimeException("Arrived at wrong parent, " + root.value);
            }
            return;
        }

        int rightDepthOfLeftSubtree = getRightDepth(root.left);
        if (rightDepthOfLeftSubtree + 1 == depth - 1) {
            insert(root.left, toInsert, depth - 1);
        } else if (rightDepthOfLeftSubtree + 1 == depth) {
            insert(root.right, toInsert, depth - 1);
        } else {
            throw new RuntimeException("Something went wrong at Node, " + root.value);
        }
    }

    private static void insertBelowLeftMostLeaf(Node root, int toInsert) {
        Node pointer = root;
        while (pointer.left != null) {
            pointer = pointer.left;
        }

        pointer.left = new Node(toInsert);
    }

    private static int getLeftDepth(Node root) {

        if (root == null) {
            return 0;
        }

        Node pointer = root;
        int depth = 0;

        while (pointer != null) {
            pointer = pointer.left;
            depth++;
        }

        return depth;
    }

    private static int getRightDepth(Node root) {
        Node pointer = root;
        int depth = 0;

        while (pointer != null) {
            pointer = pointer.right;
            depth++;
        }

        return depth;
    }

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int value) {
            this.value = value;
        }
    }
}
