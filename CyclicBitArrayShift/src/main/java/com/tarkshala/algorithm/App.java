package com.tarkshala.algorithm;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        int number = 7881;
        int bitCount = 16;
        boolean left = false;
        int places = 3;

        int biggestNumber = (int) Math.pow(2, bitCount);
        for (int i = 0; i < places; i++) {
            if (left) {
                number *= 2;
                if (number > biggestNumber) {
                    number %= biggestNumber;
                    number += 1;
                }
            } else {
                boolean rightMostBitSet = number%2 == 1;
                if (rightMostBitSet) {
                    number = (int) Math.pow(2, bitCount - 1) + number/2;
                } else {
                    number = number/2;
                }
            }
        }
        System.out.println("The number is: " + number);
    }
}
