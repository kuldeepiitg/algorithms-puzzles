package com.tarkshala.algorithms;

import java.util.LinkedList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        long start = System.currentTimeMillis();
        LinkedList list = new LinkedList();

        int number = 50;
        for (int i = 2; i <= number; i++) {
            list.add(i);
        }

        Node master = list.head;
        Node slave = list.head;

        while (master != null) {
            while (slave.next != null) {
                if (slave.next.value % master.value == 0) {
                    slave.removeNext();
                } else {
                    slave = slave.next;
                }
            }
            master = master.next;
            slave = master;
        }

        System.out.println(list);
        System.out.println(System.currentTimeMillis() - start);
    }

    public static final class Node {
        private int value;
        private Node next;

        public Node(int value) {
            this.value = value;
        }

        public void insert(int value) {
            Node newNode = new Node(value);
            newNode.next = this.next;
            this.next = newNode;
        }

        public void removeNext() {
            if (next != null) {
                this.next = next.next;
            }
        }

        public int getValue() {
            return value;
        }
    }

    public static final class LinkedList {
        private Node head;
        private Node tail;

        public void add(int number) {
            if (head == null) {
                head = new Node(number);
                tail = head;
                return;
            }

            tail.next = new Node(number);
            tail = tail.next;
        }

        @Override
        public String toString() {
            Node pointer = head;
            String representation = "[";
            while (pointer != null) {
                representation += pointer.value;
                if (pointer.next != null) {
                    representation += ", ";
                }
                pointer = pointer.next;
            }
            representation += "]";
            return representation;
        }
    }
}
