package com.tarkshala.algorithms;

import java.util.HashMap;
import java.util.Map;

public class App 
{
    private static final String MIN_OF_TREE = "min";
    private static final String MAX_OF_TREE = "max";
    private static final String IS_BST = "is_bst";

    public static void main( String[] args )
    {

        Node tree = Node.builder()
                .data(20)
                .left(Node.builder()
                        .data(10)
                        .left(Node.builder().data(5).build())
                        .right(Node.builder()
                                .data(15)
                                .left(Node.builder().data(12).build())
                                .build())
                        .build())
                .right(Node.builder()
                        .data(30)
                        .left(Node.builder()
                                .data(25)
                                .right(Node.builder().data(28).build())
                                .build())
                        .right(Node.builder().data(35).build())
                        .build())
                .build();

        if ((boolean)checkIfBST(tree).get(IS_BST)) {
            System.out.println("Tree is BST");
        } else {
            System.out.println("Tree is NOT BST");
        }
    }

    private static Map<String, Object> checkIfBST(Node tree) {

        int min = tree.getData();
        int max = tree.getData();
        boolean is_bst = true;

        if (tree.getLeft() != null) {
            Map<String, Object> leftSubtreeResult = checkIfBST(tree.getLeft());
            int leftMin = (int) leftSubtreeResult.get(MIN_OF_TREE);
            int leftMax = (int) leftSubtreeResult.get(MAX_OF_TREE);
            boolean leftIsBST = (boolean) leftSubtreeResult.get(IS_BST);
            is_bst &= leftIsBST;
            is_bst &= (leftMax < tree.getData());

            min = leftMin;
        }

        if (tree.getRight() != null) {
            Map<String, Object> rightSubtreeResult = checkIfBST(tree.getRight());
            int rightMin = (int) rightSubtreeResult.get(MIN_OF_TREE);
            int rightMax = (int) rightSubtreeResult.get(MAX_OF_TREE);
            boolean rightIsBST = (boolean) rightSubtreeResult.get(IS_BST);
            is_bst &= rightIsBST;
            is_bst &= (rightMin > tree.getData());

            max = rightMax;
        }

        Map<String, Object> result = new HashMap<>();
        result.put(MIN_OF_TREE, min);
        result.put(MAX_OF_TREE, max);
        result.put(IS_BST, is_bst);

        return result;
    }
}
