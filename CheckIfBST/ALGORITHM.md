Check for BST 
=====

Statement: Given a binary tree. Check whether it is a BST or not.
           Note: We are considering that BSTs can not contain duplicate Nodes.
           

Resource: https://practice.geeksforgeeks.org/problems/check-for-bst/1/
