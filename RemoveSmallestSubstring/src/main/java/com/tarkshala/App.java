package com.tarkshala;

import com.google.common.collect.MinMaxPriorityQueue;

import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

    }

    public Pair<Integer> getSmallestStringEnds(String word) {

        Set<Character> charactersToConsider = new HashSet<>();

        Map<Character, List<Integer>> frontEnds = new HashMap<>();
        for (int i = 0; i < word.length(); i++) {
            Character currentChar = word.charAt(i);
            charactersToConsider.add(currentChar);

            if (!frontEnds.containsKey(currentChar)) {
                frontEnds.put(currentChar, new ArrayList<Integer>());
            }
            frontEnds.get(currentChar).add(i);
            if (frontEnds.get(currentChar).size() >= 4) {
                break;
            }
        }

        Map<Character, List<Integer>> backEnds = new HashMap<>();
        for (int i = word.length() - 1; i >= 0; i--) {
            Character currentChar = word.charAt(i);
            charactersToConsider.add(currentChar);

            if (!backEnds.containsKey(currentChar)) {
                backEnds.put(currentChar, new ArrayList<Integer>());
            }
            backEnds.get(currentChar).add(i);
            if (backEnds.get(currentChar).size() >= 4) {
                break;
            }
        }

        MinMaxPriorityQueue<Integer> frontEndHeap = MinMaxPriorityQueue.maximumSize(64).expectedSize(26).create();
        MinMaxPriorityQueue<Integer> backEndHeap = MinMaxPriorityQueue.maximumSize(64).expectedSize(26).create();
        for (char c: charactersToConsider) {
            if (frontEnds.containsKey(c) && !backEnds.containsKey(c)) {
                List<Integer> fronts = frontEnds.get(c);
                if (fronts.size() == 4) {
                    frontEndHeap.add(fronts.get(3));
                }
            } else if (!frontEnds.containsKey(c) && backEnds.containsKey(c)) {
                List<Integer> backs = backEnds.get(c);
                if (backs.size() == 4) {
                    backEndHeap.add(backs.get(3));
                }
            } else {
                List<Integer> fronts = frontEnds.get(c);
                List<Integer> backs = backEnds.get(c);

                Pair<Integer> smallestRemovableSegment = null;
                if (fronts.size() == 4) {
                    smallestRemovableSegment = Pair.builder().key(fronts.get(3)).value(word.length() - 1).build();
                }

                if (backs.size() == 4) {
                    if (smallestRemovableSegment == null ||
                            smallestRemovableSegment.getValue() - smallestRemovableSegment.getKey() > backs.get(3) - 0){
                        smallestRemovableSegment = Pair.builder().key(0).value(backs.get(3)).build();
                    }
                }

                // TODO: 14/06/21 Get all segments, and compare which is smallest
                // iterate over front, get it's counterpart from back
                // add smallest's front and back to heaps
            }
        }

        return Pair.builder().key(frontEndHeap.peekLast()).value(backEndHeap.peekFirst()).build();
    }
}
