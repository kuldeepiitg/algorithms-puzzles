package com.tarkshala;

import java.util.Map;

public class Pair<T> implements Map.Entry<T, T> {

    private T key;
    private T value;

    public Pair(T key, T value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public T getKey() {
        return key;
    }

    @Override
    public T getValue() {
        return value;
    }

    public T setKey(T key) {
        T oldKey = this.key;
        this.key = key;
        return oldKey;
    }

    @Override
    public T setValue(T value) {
        T oldValue = this.value;
        this.value = value;
        return oldValue;
    }

    public static PairBuilder builder() {
        return new PairBuilder();
    }

    public static class PairBuilder<T> {
        T key;
        T value;

        public PairBuilder<T> key(T key) {
            this.key = key;
            return this;
        }

        public PairBuilder<T> value(T value) {
            this.value = value;
            return this;
        }

        public Pair<T> build() {
            return new Pair<>(key, value);
        }
    }

}
