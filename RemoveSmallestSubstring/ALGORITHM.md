Remove Substring
=======

You are given a string S containing only lowercase letters(a-z), of length N.

Find the minimum size of the substring, you need to remove from string S such that the remaining characters in string S occur for at most 3 times.

Note
========
The minimum size of the substring to be removed can be zero also, i.e. you may not remove a substring. It depends on the input. 
