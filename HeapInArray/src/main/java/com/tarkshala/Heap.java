package com.tarkshala;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Setter
@Getter
public class Heap<T> {

    private Comparator<T> comparator;
    private T[] array;
    private int size;

    public Heap(Comparator<T> comparator, T[] array) {
        this.comparator = comparator;
        this.array = array;
        this.size = 0;
    }

    public void insert(T element) {
        size++;
        array[size] = element;
        bubbleUp(size);
    }

    public T pop() {
        T top = array[1];
        if (size == 1) {
            size--;
        } else {
            array[1] = array[size];
            size--;
            sinkDown(1);
        }
        return top;
    }

    private void bubbleUp(int index) {
        while (index > 1 && comparator.compare(array[index], array[index/2]) > 0) {
            T temp = array[index/2];
            array[index/2] = array[index];
            array[index] = temp;
            index = index/2;
        }
    }

    private void sinkDown(int index) {
        while (true) {
            if (index*2 <= size && (index*2 + 1) <= size) {
                if (comparator.compare(array[index*2], array[index*2 + 1]) > 0) {
                    if (comparator.compare(array[index*2], array[index]) > 0) {
                        T temp = array[index*2];
                        array[index*2] = array[index];
                        array[index] = temp;
                        index = index*2;
                    } else {
                        break;
                    }
                } else {
                    if (comparator.compare(array[index*2 + 1], array[index]) > 0) {
                        T temp = array[index*2 + 1];
                        array[index*2 + 1] = array[index];
                        array[index] = temp;
                        index = index*2 + 1;
                    } else {
                        break;
                    }
                }
            } else if (index*2 <= size) {
                if (comparator.compare(array[index*2], array[index]) > 0) {
                    T temp = array[index*2];
                    array[index*2] = array[index];
                    array[index] = temp;
                    index = index*2;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }

    public T peek() {
        return array[1];
    }
}
