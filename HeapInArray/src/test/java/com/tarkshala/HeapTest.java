package com.tarkshala;

import org.junit.Test;

import java.util.Comparator;

public class HeapTest {

    @Test
    public void insert() {

        Comparator<Integer> comparator = Comparator.comparingInt(integer -> integer);
        Heap<Integer> heap = new Heap<Integer>(comparator, new Integer[1024]);
        heap.insert(10);
        System.out.println(heap.peek());

        heap.insert(20);
        System.out.println(heap.peek());

        heap.insert(30);
        System.out.println(heap.peek());

        heap.insert(40);
        System.out.println(heap.peek());

        heap.insert(50);
        System.out.println(heap.peek());

        heap.insert(60);
        System.out.println(heap.peek());

        heap.insert(70);
        System.out.println(heap.peek());

        heap.insert(80);
        System.out.println(heap.peek());

        heap.pop();
        System.out.println(heap.peek());

        heap.pop();
        System.out.println(heap.peek());

        heap.pop();
        System.out.println(heap.peek());

        heap.pop();
        System.out.println(heap.peek());

        heap.pop();
        System.out.println(heap.peek());

        heap.pop();
        System.out.println(heap.peek());

        heap.pop();
        System.out.println(heap.peek());

    }
}
