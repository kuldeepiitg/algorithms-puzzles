Statement
=====
https://www.geeksforgeeks.org/largest-rectangle-under-histogram/

Largest Rectangular Area in a Histogram | Set 2
Find the largest rectangular area possible in a given histogram where the largest rectangle can be made of a number of contiguous bars. For simplicity, assume that all bars have same width and the width is 1 unit.

For example, consider the following histogram with 7 bars of heights {6, 2, 5, 4, 5, 1, 6}. The largest possible rectangle possible is 12 (see the below figure, the max area rectangle is highlighted in red)

![hitogram1.png](https://media.geeksforgeeks.org/wp-content/cdn-uploads/histogram1.png)

Solution 1 (Original)
======

Keep track of rectangles(of every possible height) than can be formed by previous bars, 

## Pseudo code

Maintain an array to note the previous depths. 

```
for each bar:
    for i = 0 to bar.height:
        increment array[i] by 1;
        if rectangle formed so far is greater, then update maxArea with new value.
    for i = bar.height+1 to maxBarLength
        array[i] = 0;
```

Time complexity: ```O(m*n)```, where m is max height of a bar, n is number of bars.

Space complexity: ```O(m)```

Solution 2
====
Using stack, popular solution across internet

Check https://www.geeksforgeeks.org/largest-rectangle-under-histogram/

Time Complexity: ```O(n)```

Aux Space Complexity: ```O(n)```
 
