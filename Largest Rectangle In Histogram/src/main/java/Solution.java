public class Solution {

    public static void main(String[] args) {
        int[] histogram = {2, 1, 5, 6, 2, 3};
        int area = HistogramComputer.largestRectangle(histogram);
        System.out.println(HistogramComputer.largestRectangleUsingStack(histogram));
        System.out.println(area);
        area = HistogramComputer.largestRectangle(new int[]{2,3,4,5,3,1,4,6});
        System.out.println(area);
        System.out.println(HistogramComputer.largestRectangleUsingStack(new int[]{2,3,4,5,3,1,4,6}));

    }
}
