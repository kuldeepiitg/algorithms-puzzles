import java.util.Stack;

public class HistogramComputer {

    public static int largestRectangle(int[] histogram) {

        int maxHeight = 0;
        for (int i = 0; i < histogram.length; i++) {
            if (maxHeight < histogram[i]) {
                maxHeight = histogram[i];
            }
        }

        int[] continuousPossibleHeights = new int[maxHeight+1];
        int maxArea = 0;

        for (int i = 0; i < histogram.length; i++) {
            int j;
            for (j = 1; j <= histogram[i]; j++) {
                continuousPossibleHeights[j]++;
                if (maxArea < j * continuousPossibleHeights[j]) {
                    maxArea = j * continuousPossibleHeights[j];
                }
            }
            while (j < continuousPossibleHeights.length && continuousPossibleHeights[j] > 0) {
                continuousPossibleHeights[j] = 0;
                j++;
            }
        }

        return maxArea;
    }


    // FIXME: 10/05/20 Incorrect outputs, wrong algo perhaps
    public static int largestRectangleUsingStack(int[] histogram) {

        if (histogram.length == 0) {
            return 0;
        } else if (histogram.length == 1) {
            return histogram[0];
        }

        int maxArea = 0;
        Stack<Integer> stack = new Stack<>();
        stack.push(0);
        for (int i = 1; i < histogram.length; i++) {

            if (histogram[i] < histogram[stack.peek()]) {
                while (!stack.isEmpty() && histogram[i] < histogram[stack.peek()]) {
                    if (maxArea < histogram[stack.peek()]*(i-stack.peek())) {
                        maxArea = histogram[stack.peek()]*(i-stack.peek());
                    }
                    stack.pop();
                }
            }
            stack.add(i);
        }

        while (!stack.isEmpty()) {
            if (maxArea < histogram[stack.peek()]*(histogram.length - stack.peek())) {
                maxArea = histogram[stack.peek()]*(histogram.length - stack.peek());
            }
            stack.pop();
        }

        return maxArea;
    }
}
