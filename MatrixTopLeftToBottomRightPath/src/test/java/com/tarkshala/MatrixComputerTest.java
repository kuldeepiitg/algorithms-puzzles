package com.tarkshala;

public class MatrixComputerTest {

    @org.junit.Test
    public void findPathTo() {

        MatrixComputer matrixComputer = new MatrixComputer(new int[10][10]);
        matrixComputer.block(1,0);
        matrixComputer.block(1,1);
        matrixComputer.block(1,2);
        matrixComputer.block(1,3);
        matrixComputer.block(1,4);
        matrixComputer.block(1,5);
        matrixComputer.block(1,6);
        matrixComputer.block(1,7);
        matrixComputer.block(1,8);
        matrixComputer.block(3,1);
        matrixComputer.block(3,2);
        matrixComputer.block(3,3);
        matrixComputer.block(3,4);
        matrixComputer.block(3,5);
        matrixComputer.block(3,6);
        matrixComputer.block(3,7);
        matrixComputer.block(3,8);
        matrixComputer.block(3,9);

        matrixComputer.findPathTo(9,9);
    }
}
