package com.tarkshala;

import lombok.Setter;

import java.util.LinkedList;
import java.util.Queue;

public class MatrixComputer {

    private int[][] matrix;

    public MatrixComputer(int[][] matrix) {
        this.matrix = matrix;
    }

    public void block(int y, int x) {
        matrix[y][x] = -1;  // marked blocked
    }

    public void findPathTo(int y, int x) {

        Queue<Node> queue = new LinkedList<Node>();
        queue.add(new Node(0,0, "(0,0)"));
        matrix[0][0] = 1;   // marked reached

        while (!queue.isEmpty()) {
            Node node = queue.poll();
            if (node.y == y && node.x == x) {
                System.out.println(node.path);
                return;
            }

            // Go up
            if (node.y < matrix.length - 1 && matrix[node.y + 1][node.x] != -1 && matrix[node.y + 1][node.x] != 1) {
                String path = node.path + "->(" + String.valueOf(node.y + 1) + "," + String.valueOf(node.x) + ")";
                queue.add(new Node(node.y + 1, node.x, path));
                matrix[node.y + 1][node.x] = 1;
            }

            // Go right
            if (node.x < matrix[0].length - 1 && matrix[node.y][node.x + 1] != -1 && matrix[node.y][node.x + 1] != 1) {
                String path = node.path + "->(" + String.valueOf(node.y) + "," + String.valueOf(node.x + 1) + ")";
                queue.add(new Node(node.y, node.x + 1, path));
                matrix[node.y][node.x + 1] = 1;
            }

            // Go down
            if (node.y > 0 && matrix[node.y - 1][node.x] != -1 && matrix[node.y - 1][node.x] != 1) {
                String path = node.path + "->(" + String.valueOf(node.y-1) + "," + String.valueOf(node.x) + ")";
                queue.add(new Node(node.y - 1, node.x, path));
                matrix[node.y - 1][node.x] = 1;
            }

            // Go left
            if (node.x > 0 && matrix[node.y][node.x - 1] != -1 && matrix[node.y][node.x - 1] != 1) {
                String path = node.path + "->(" + String.valueOf(node.y) + "," + String.valueOf(node.x - 1) + ")";
                queue.add(new Node(node.y, node.x - 1, path));
                matrix[node.y][node.x - 1] = 1;
            }
        }

        System.out.println("No path possible");
    }

    public static class Node {

        private int x;
        private int y;

        @Setter
        private String path;

        public Node(int y, int x, String path) {
            this.x = x;
            this.y = y;
            this.path = path;
        }
    }
}
