Statement
=====
Find path to reach from one corner of 2d matrix to the diagonally opposite i.e. 0,0 to n,n. Allowed to move in any direction (top, down, left, right), there could be some cells which don't allow movement.  

Solution
===
Use BFS traversal.
