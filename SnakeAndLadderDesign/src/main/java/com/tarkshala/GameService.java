package com.tarkshala;

import com.tarkshala.models.Game;
import com.tarkshala.models.SnakeBoard;

import java.util.Random;

public class GameService {

    private Game game;

    private int turn;

    public GameService() {
        this.game = new Game(SnakeBoard.generateSnakeBoard());
    }

    public void addPlayer(String username) {
        this.game.addPlayer(username);
    }

    /**
     * Throw a dice and move player as per the game
     * @return final square where the player reached
     */
    public void nextTurn() {

        do {
            turn = (turn+1) % game.getPlayerStates().size();
        } while (game.getPlayerStates().get(turn).hasWon());

        Game.PlayerState playerState = game.getPlayerStates().get(turn);
        Random random = new Random();
        int numberOnDice = 1 + random.nextInt(6);
        int square = game.updatePlayerState(playerState.getUsername(), numberOnDice);
        System.out.println("Player " + playerState.getUsername() + " got " + numberOnDice + " and reached " + square);
        if (square == 100) {
            System.out.println("Player " + playerState.getUsername() + " won the game");
        }
        return;
    }

    public boolean hasGameFinished() {
        boolean isFinished = true;
        for (int i = 0; i < game.getPlayerStates().size(); i++) {
            isFinished &= game.getPlayerStates().get(i).hasWon();
        }

        return isFinished;
    }

}
