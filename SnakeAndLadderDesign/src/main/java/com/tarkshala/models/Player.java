package com.tarkshala.models;

public class Player {

    private String name;

    private String email;

    private String phone;

    private String username;

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }
}
