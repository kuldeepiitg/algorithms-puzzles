package com.tarkshala.models;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private List<PlayerState> playerStates;

    private SnakeBoard snakeBoard;

    public Game(SnakeBoard snakeBoard) {
        this.snakeBoard = snakeBoard;
        this.playerStates = new ArrayList<>();
    }

    public void addPlayer(String username) {
        playerStates.add(new PlayerState(username));
    }

    public int updatePlayerState(String username, int move) {

        PlayerState playerState = getPlayerState(username);
        int currentSquare = playerState.getSquare();
        if (currentSquare + move <= 100) {
            currentSquare += move;
        }

        currentSquare = snakeBoard.getEndSquare(currentSquare);
        playerState.setSquare(currentSquare);

        return currentSquare;
    }

    public List<PlayerState> getPlayerStates() {
        return playerStates;
    }

    private PlayerState getPlayerState(String username) {
        PlayerState playerState = null;
        for (int i = 0; i < playerStates.size(); i++) {

            if (playerStates.get(i).username == username)
                playerState = playerStates.get(i);
        }

        return playerState;
    }

    public static class PlayerState {

        private String username;

        private int square;

        public PlayerState(String username) {
            this.username = username;
            this.square = 0;    // initially all players are supposed to remain at start(0).
        }

        public String getUsername() {
            return username;
        }

        public int getSquare() {
            return square;
        }

        public void setSquare(int square) {
            this.square = square;
        }

        public boolean hasWon() {
            return square == 100;
        }
    }
}
