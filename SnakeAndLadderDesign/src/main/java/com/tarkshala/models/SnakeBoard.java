package com.tarkshala.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class SnakeBoard {

    private List<Transition> transitions;

    public SnakeBoard() {
        this.transitions = new ArrayList<Transition>();
    }

    public void add(Transition transition) {
        transitions.add(transition);
        transitions.sort(Comparator.comparingInt(Transition::getStart));
    }

    public void add(List<Transition> transitions) {
        transitions.addAll(transitions);
        transitions.sort(Comparator.comparingInt(Transition::getStart));
    }

    public int getEndSquare(int startSquare) {
        for (Transition transition: transitions) {
            if (transition.getStart() == startSquare) {
                return transition.getEnd();
            }
        }

        return startSquare;
    }

    public static class Transition {

        private int start;

        private int end;

        public Transition(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }
    }

    public static SnakeBoard generateSnakeBoard() {

        SnakeBoard snakeBoard = new SnakeBoard();
        for (int i = 0; i < 20; i++) {
            Random random = new Random();
            random.setSeed(System.currentTimeMillis());
            int start = random.nextInt(99);
            int end = random.nextInt(99);

            snakeBoard.add(new SnakeBoard.Transition(start, end));
        }

        return snakeBoard;
    }
}
