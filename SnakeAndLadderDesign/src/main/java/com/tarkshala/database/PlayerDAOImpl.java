package com.tarkshala.database;

import com.tarkshala.database.models.PlayerDO;

import java.util.Map;
import java.util.Optional;

public class PlayerDAOImpl implements DAO<PlayerDO> {

    private Map<String, PlayerDO> table;

    @Override
    public void create(PlayerDO entity) {
        entity.setCreationTime(System.currentTimeMillis());
        table.put(entity.getUid(), entity);
    }

    @Override
    public Optional<PlayerDO> read(String uid) {
        return Optional.ofNullable(table.get(uid));
    }

    @Override
    public void update(PlayerDO entity) {
        entity.setUpdateTime(System.currentTimeMillis());
        table.put(entity.getUid(), entity);
    }

    @Override
    public void delete(String uid) {
        table.remove(uid);
    }
}
