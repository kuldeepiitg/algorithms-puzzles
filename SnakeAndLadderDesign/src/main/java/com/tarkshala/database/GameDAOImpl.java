package com.tarkshala.database;

import com.tarkshala.database.models.EntityDO;
import com.tarkshala.database.models.GameDO;

import java.util.Map;
import java.util.Optional;

public class GameDAOImpl<T extends EntityDO> implements DAO<GameDO> {

    private Map<String, GameDO> table;

    @Override
    public void create(GameDO entity) {
        entity.setCreationTime(System.currentTimeMillis());
        table.put(entity.getUid(), entity);
    }

    @Override
    public Optional<GameDO> read(String uid) {
        return Optional.ofNullable(table.get(uid));
    }

    @Override
    public void update(GameDO entity) {
        entity.setUpdateTime(System.currentTimeMillis());
        table.put(entity.getUid(), entity);
    }

    @Override
    public void delete(String uid) {
        table.remove(uid);
    }
}
