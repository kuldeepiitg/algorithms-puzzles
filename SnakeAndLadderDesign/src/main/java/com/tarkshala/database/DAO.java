package com.tarkshala.database;

import com.tarkshala.database.models.EntityDO;

import java.util.Optional;

public interface DAO<T extends EntityDO> {

    void create(T entity);

    Optional<T> read(String uid);

    void update(T entity);

    void delete(String uid);
}
