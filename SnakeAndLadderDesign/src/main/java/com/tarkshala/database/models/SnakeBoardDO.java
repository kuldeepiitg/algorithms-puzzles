package com.tarkshala.database.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SnakeBoardDO {

    private List<SnakeBoardDO.Transition> transitions;

    public SnakeBoardDO() {
        this.transitions = new ArrayList<>();
    }

    public void add(SnakeBoardDO.Transition transition) {
        transitions.add(transition);
        transitions.sort(Comparator.comparingInt(SnakeBoardDO.Transition::getStart));
    }

    public void add(List<SnakeBoardDO.Transition> transitions) {
        transitions.addAll(transitions);
        transitions.sort(Comparator.comparingInt(SnakeBoardDO.Transition::getStart));
    }

    private static class Transition {

        private int start;

        private int end;

        public Transition(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }
    }
}
