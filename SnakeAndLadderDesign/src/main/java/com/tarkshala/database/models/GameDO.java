package com.tarkshala.database.models;

import java.util.List;

public class GameDO extends EntityDO {

    public GameDO(String uid) {
        super(uid);
    }

    private List<GameDO.PlayerState> playerStates;

    private SnakeBoardDO snakeBoard;

    public void addPlayer(PlayerDO player) {
        playerStates.add(new GameDO.PlayerState(player.getUsername()));
    }

    private static class PlayerState {

        private String username;

        private int square;

        private PlayerState(String username) {
            this.username = username;
            this.square = 0;    // initially all players are supposed to remain at start(0).
        }

        public String getUsername() {
            return username;
        }

        public int getSquare() {
            return square;
        }

        public void setSquare(int square) {
            this.square = square;
        }
    }
}
