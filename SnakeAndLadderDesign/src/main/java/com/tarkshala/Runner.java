package com.tarkshala;

public class Runner {

    public static void main(String[] args) {
        GameService gameService = new GameService();
        gameService.addPlayer("kuldeep");
        gameService.addPlayer("anju");

        while (!gameService.hasGameFinished()) {
            gameService.nextTurn();
        }

    }
}
