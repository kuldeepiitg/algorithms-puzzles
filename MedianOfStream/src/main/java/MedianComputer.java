import lombok.Getter;

import java.util.PriorityQueue;

public class MedianComputer {

    private PriorityQueue<Float> minHeap = new PriorityQueue<Float>((i1, i2) -> (int) (i1 - i2));
    private PriorityQueue<Float> maxHeap = new PriorityQueue<Float>((i1, i2) -> (int) (i2 - i1));

    @Getter
    private Float median;

    private int count;

    public MedianComputer() {
        this.median = Float.MIN_VALUE;
        count = 0;
    }

    public void insert(Float element) {
        if (count == 0) {
            this.median = element;
            this.count++;
            return;
        } else if (count == 1) {
            count++;
            if (element < median) {
                maxHeap.add(element);
                minHeap.add(median);
            } else {
                maxHeap.add(median);
                minHeap.add(element);
            }
            this.median = (maxHeap.peek() + minHeap.peek())/2;
            return;
        }

        count++;
        if (element > minHeap.peek()) {
            if (count%2 == 0) {
                maxHeap.add(median);
                minHeap.add(element);
                this.median = (minHeap.peek() + maxHeap.peek())/2;
            } else {
                minHeap.add(element);
                this.median = minHeap.poll();
            }
        } else if (element < maxHeap.peek()) {
            if (count%2 == 0) {
                minHeap.add(median);
                maxHeap.add(element);
                this.median = (minHeap.peek() + maxHeap.peek())/2;
            } else {
                maxHeap.add(element);
                this.median = maxHeap.poll();
            }
        }
    }
}
