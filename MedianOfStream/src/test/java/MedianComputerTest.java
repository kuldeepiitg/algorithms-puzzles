import static org.junit.Assert.*;

public class MedianComputerTest {

    @org.junit.Test
    public void insert() {

        MedianComputer computer = new MedianComputer();

        computer.insert(1f);
        assertEquals(1f, computer.getMedian(), 0);

        computer.insert(2f);
        assertEquals(1.5f, computer.getMedian(), 0);

        computer.insert(3f);
        assertEquals(2f, computer.getMedian(), 0);

        computer.insert(4f);
        assertEquals(2.5f, computer.getMedian(), 0);

        computer.insert(5f);
        assertEquals(3f, computer.getMedian(), 0);

        computer.insert(6f);
        assertEquals(3.5f, computer.getMedian(), 0);

        computer.insert(7f);
        assertEquals(4f, computer.getMedian(), 0);

        computer.insert(8f);
        assertEquals(4.5f, computer.getMedian(), 0);

        MedianComputer computer1 = new MedianComputer();
        computer1.insert(8f);
        assertEquals(8f, computer1.getMedian(), 0);

        computer1.insert(7f);
        assertEquals(7.5f, computer1.getMedian(), 0);

        computer1.insert(6f);
        assertEquals(7f, computer1.getMedian(), 0);

        computer1.insert(5f);
        assertEquals(6.5f, computer1.getMedian(), 0);

        computer1.insert(4f);
        assertEquals(6f, computer1.getMedian(), 0);

        computer1.insert(3f);
        assertEquals(5.5f, computer1.getMedian(), 0);

        computer1.insert(2f);
        assertEquals(5f, computer1.getMedian(), 0);

        computer1.insert(1f);
        assertEquals(4.5f, computer1.getMedian(), 0);
    }
}
