package com.tarkshala;

import com.sun.tools.javac.util.Assert;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int first = Integer.MAX_VALUE;
        int second = Integer.MAX_VALUE;

        String product = multiply(first, second);
        Assert.check("004611686014132420609".equals(product));
        System.out.println(product);

        product = multiply(0, 0);
        System.out.println(product);

        product = multiply(-1, 0);
        System.out.println(product);

        product = multiply(-1, -1);
        System.out.println(product);

        product = multiply(Integer.MIN_VALUE + 1, Integer.MIN_VALUE + 1);
        System.out.println(product);

        product = multiply(-25, -25);
        System.out.println(product);
    }

    private static String multiply(int first, int second) {

        boolean isProductNegative = (first < 0) ^ (second < 0);
        if ((first == 0) || (second == 0)) {
            isProductNegative = false;
        }

        if (first < 0) {
            first *= -1;
        }

        if (second < 0) {
            second *= -1;
        }

        int firstDigitCount = first != 0 ? (int) Math.floor(Math.log10(first)) + 1 : 1;
        int[] firstArray = new int[firstDigitCount];

        int secondDigitCount = second != 0 ? (int) Math.floor(Math.log10(second)) + 1 : 1;
        int[] secondArray = new int[secondDigitCount];

        int[] outputArray = new int[firstDigitCount + secondDigitCount + 1];

        int i = 0;
        while (first > 0) {
            firstArray[i] = first%10;
            first /= 10;
            i++;
        }

        int j = 0;
        while (second > 0) {
            secondArray[j] = second%10;
            second /= 10;
            j++;
        }

        int placeValue = 0;
        for (int k = 0; k < outputArray.length; k++) {
            i = k;
            j = 0;

            while (i >= 0 && j <= k) {
                if (i >= firstDigitCount || j >= secondDigitCount) {
                    i--;
                    j++;
                    continue;
                }
                placeValue += firstArray[i] * secondArray[j];
                i--;
                j++;
            }

            outputArray[k] = placeValue % 10;
            placeValue /= 10;
        }
        String product = convert(outputArray);
        if (isProductNegative) {
            product = "-" + product;
        }

        return product;
    }

    private static String convert(int[] outputArray) {
        StringBuilder output = new StringBuilder();
        for (int value: outputArray) {
            output.insert(0, value);
        }
        return output.toString();
    }
}
