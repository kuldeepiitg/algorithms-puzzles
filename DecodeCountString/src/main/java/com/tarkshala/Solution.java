package com.tarkshala;

import java.util.Stack;

public class Solution {
    public static void main(String[] args) {

        String input = "2[a11[b]]";
        String pattern = decode(input);
        System.out.println(pattern);
    }

    private static String decode(String input) {
        Stack<Integer> countStack = new Stack<>();
        Stack<String> patternStack = new Stack<>();

        String pattern = "";
        int number = 0;
        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                number *= 10;
                number += Character.getNumericValue(input.charAt(i));
                continue;
            }

            if (input.charAt(i) == '[') {
                patternStack.push(pattern);
                pattern = "";
                countStack.push(number);
                number = 0;
                continue;
            }

            if (input.charAt(i) == ']') {
                int count = countStack.pop();
                String patternMultiplied = "";
                for (int j = 0; j < count; j++) {
                    patternMultiplied += pattern;
                }
                pattern = patternStack.pop();
                pattern += patternMultiplied;
                continue;
            }

            pattern += input.charAt(i);
        }

        return pattern;
    }
}
